<?php
include_once ('./BusinessLogicLayer.php');
use BusinessLogicLayer\BLL;
use BusinessLogicLayer\book;
use BusinessLogicLayer\furniture;
use BusinessLogicLayer\DVD;

$data = json_decode(file_get_contents('php://input'), true);
if (isset($data['action'])&& $data['action']=="fetchall") { 

    $Products = BLL::GetAllProducts();
 
    $Data = array();
    foreach ($Products as $product) {
        $object =  (object) $product->GetData();
        $Data[] = $object;
    }
    $text = json_encode($Data);
    echo $text;
}
if (isset($data['action'])&&  $data['action']=='Delete'&&isset( $data['DelProds'])) {
    DeleteProducts($data['DelProds']);
}
if (isset($data['InsProd'])) {
    $type = "BusinessLogicLayer"."\\".$data['InsProd']['Type'];
    $product = new $type($data['InsProd']['Sku']);
    if ($product->Fill($data['InsProd'])) {
        if (!$product->AddToDb($Error_Message))
            echo $Error_Message;
        }
}
function  DeleteProducts($Data)
{
    foreach ($Data as $value){
        GetSkuAndTypeFromText($value,$Sku,$Type);
        $product = new  $Type($Sku);
        $product->DeleteFromDB();
    }
}
function  GetSkuAndTypeFromText($value,&$Sku,&$Type)
{
    $position = strripos($value, ",");
    $Sku = substr($value, 0, $position);
    $Type = "BusinessLogicLayer"."\\".substr($value, $position+1);
}
    