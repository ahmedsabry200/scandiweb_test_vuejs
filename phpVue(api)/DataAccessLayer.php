<?php
namespace DataAccessLayer;

use BusinessLogicLayer\book;
use BusinessLogicLayer\DVD;
use BusinessLogicLayer\furniture;
use BusinessLogicLayer\BLL;
use PreparedStatements\preStat;
use mysqli;

class DAL
{
    private static $error_no;
    private static $affected_rows;
    public static function Get_error()
    {
       return self::$error_no;
    }
    public static function Get_affected_rows()
    {
       return self::$affected_rows;
    }
    //-------------------Connect to database------------------------------
    private static function ConnectToDb()
    {
        self::$error_no = 0;
        self::$affected_rows = 0;
        // Username is root
        $user = 'ScandiwebRoot';
        $password = 'scandiweb2022'; 
        // Database name is scandiweb_db
        $database = 'scandiwebDB'; 
        // Server is localhost 
        $servername = 'localhost';
        $mysqli = new mysqli($servername, $user,  $password, $database);
        // Checking for connections errors
        if ($mysqli->connect_error) 
        {
            die('Connect Error (' .$mysqli->connect_errno . ') '. $mysqli->connect_error);
        }
        return $mysqli;
    }
    //Dynamically Execute  pepared statement to prevent sql injection
    private static function ExecuteSql($SqlStatement, $params = null)
    {
        $connection = self::ConnectToDb();  
        $stmt = $connection->prepare($SqlStatement);
        if ($params != null &&count($params) != 0) {
            $Parameters_DataTypes = "";
            $Parameters_Names = "";
            $Parameters_Values = "";
            foreach ($params as $Param ) {
                $Parameters_DataTypes .= $Param->GetType();
                $Parameters_Names .= ",". $Param->GetName();
                if ($Param->GetType() != "s")
                    $Parameters_Values.=$Param->GetName()."=". $Param->GetValue().";";
                else
                    $Parameters_Values.=$Param->GetName().'=\''. $Param->GetValue().'\';';
            }
            $str = "\$stmt-> bind_param('".$Parameters_DataTypes."'".$Parameters_Names.");";
            eval($str) ;
            eval($Parameters_Values);
        }
        $stmt->execute();
        self::$error_no=$stmt->errno;
        self::$affected_rows=$stmt->affected_rows;
        $result = $stmt->get_result();
        $stmt->close();
        return  $result;
    }
    //***************************AllProducts****************************/
    //get all products from database
    public static function GetAllProducts()
    {
        $SqlStatement="SELECT product_sku,product_name,price,Type
                       FROM products
                       order by products.product_sku;";
        $result = self::ExecuteSql($SqlStatement);
        $Products = array();
        while( $row=$result->fetch_assoc()) {
            //create an object from the product
            $p = "BusinessLogicLayer"."\\".$row['Type'];
            $Product = new  $p($row['product_sku']);
            $Product->fill();
            $Products[] = $Product;
        }
        return $Products;
    }
    //Get product by specific SKU
    public static function GetProductBySku($product)
    {
        $SqlStatement = "SELECT product_sku,product_name,price,Type
                         FROM products
                         where product_sku =?";
        //create sku parameter
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($product->GetSku());
        $params[] = $param;
        //Get the result
        $result = self::ExecuteSql($SqlStatement,$params);
        $row = $result->fetch_assoc(); 
        if ($row) {
            //fill product with other data
            $product->fill($row['product_sku']);
            return true;
        }
        return false;
    }
    //insert a product in the database
    private static function InsertProduct($product)
    {
        $SqlStatement="insert into products( product_sku,product_name,price,Type)
                       Values (?,?,?,?)";
        //--------------create parameters---------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($product->GetSku());
        $params[]= $param;
        //--------------product_name parameter-------------------------------------
        $param1 = new SQLParameter();
        $param1->SetName("product_name");
        $param1->SetType("s");
        $param1->SetValue($product->GetName());
        $params[] = $param1;
        //---------------price parameter-------------------------------------
        $param2 = new SQLParameter();
        $param2->SetName("price");
        $param2->SetType("d");
        $param2->SetValue($product->GetPrice());
        $params[]= $param2;
        //---------------Type parameter-------------------------------------
        $param3 = new SQLParameter();
        $param3->SetName("Type");
        $param3->SetType("s");
        $param3->SetValue($product->GetType());
        $params[]= $param3;
        //----------------Insert product----------------------
        self::Executesql($SqlStatement, $params);
        if (self::$affected_rows>0)
            return true;
        else
            return false;
    }
    //delete product from database
    public static function DeleteProduct($product)
    {
        $SqlStatement = "Delete from products where product_sku=?";
        //--------------create sku parameter---------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($product->GetSku());
        $params[] = $param;
        //--------------Delete product------------------------
        self::Executesql($SqlStatement, $params);
        if(self::$affected_rows>0)
            return true;
        else
            return false; 
    }
    //Get count of all products or check existance of specific product
    public static function ProductsCount($product=null)
    {
        $SqlStatement = "select count(product_sku) as c from products ";
        if ($product != null ){  
            $SqlStatement .= "where product_sku=?";
            //--------------create sku parameter---------------------
            $param = new SQLParameter();
            $param->SetName("sku");
            $param->SetType("s");
            $param->SetValue($product->GetSku());
            $params[] = $param;
        }
        //----------------Get products count-----------------
        return  self::Executesql($SqlStatement,$params);
    }
    //***************************Books********************************** */
    //here we will return all books in the Database
    public static function GetAllBooks()
    {
        $SqlStatement = "SELECT product_sku,product_name,price,weight
                       FROM books,products
                       where books.books_sku=products.product_sku;";

        //--------------Get All Books----------------------------
        $result = self::ExecuteSql($SqlStatement);
        while ($row=$result->fetch_assoc()){
            $b = new  book($row['product_sku'], $row['product_name'], $row['price'], $row['weight']);
            $books[] = $b;
        }
        return $books;
    }  
    //here we return specific book by its Sku
    public static function GetBookBySku($Book)
    {
        $SqlStatement = "SELECT product_sku,product_name,price,Type,weight
        FROM  books,products
        where books.books_sku=products.product_sku
        and   books.books_sku=?";
        //------------create sku parameter-----------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Book->GetSku());
        $params[] = $param;
        //---------------Get Book---------------------------
        $result = self::ExecuteSql($SqlStatement,$params);
        $row = $result->fetch_assoc();
        if ($row) {
        //----------fill Book with other data -----------
            $Book->SetName($row['product_name']) ;
            $Book->SetPrice($row['price']) ;
            $Book->setWeight($row['weight']) ;
            return true;
        } 
        return false;
    }
    //insert a new Book in the database
    public static function InsertBook($Book)
    {
        //first insert this book in product table 
        if (self::InsertProduct($Book)){
            //if it was inserted successfully insert it in books table
            $SqlStatement = "insert into Books(books_sku,weight)
                           Values (?,?)";
            //---------------sku parameter-----------------------------------
            $param = new SQLParameter();
            $param->SetName("sku");
            $param->SetType("s");
            $param->SetValue($Book->GetSku());
            $params[] = $param;
            //-----------------weight parameter----------------------------------
            $param1 = new SQLParameter();
            $param1->SetName("weight");
            $param1->SetType("d");
            $param1->SetValue($Book->GetWeight());
            $params[] = $param1;
            //------------------pass to execute----------------------------------  
            self::Executesql($SqlStatement,$params);
            if(self::$affected_rows>0)
                return true;
        }
        return false;
    }
    //insert a new Book in the database by using Stored procedure
    public static function InsertBookBySP($Book)
    {
        $SqlStatement = "CALL InsertBook (?,?,?,?,?)";
        //----------------sku parameter-----------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Book->GetSku());
        $params[] = $param;
        //----------------product_name parameter-----------------------------------
        $param1 = new SQLParameter();
        $param1->SetName("product_name");
        $param1->SetType("s");
        $param1->SetValue($Book->GetName());
        $params[] = $param1;
        //----------------price Parameter------------------------------------
        $param2 = new SQLParameter();
        $param2->SetName("price");
        $param2->SetType("d");
        $param2->SetValue($Book->GetPrice());
        $params[] = $param2;
        //----------------Type Parameter------------------------------------
        $param3 = new SQLParameter();
        $param3->SetName("Type");
        $param3->SetType("s");
        $param3->SetValue($Book->GetType());
        $params[] = $param3;
        //-----------------weight parameter----------------------------------
        $param4 = new SQLParameter();
        $param4->SetName("weight");
        $param4->SetType("d");
        $param4->SetValue($Book->GetWeight());
        $params[] = $param4;
        //------------------pass to execute----------------------------------  
        self::Executesql($SqlStatement, $params);
        if(self::$affected_rows>0)
            return true;
        return false;
    }
    //delete Book from database
    public static function DeleteBook($Book)
    {
        $SqlStatement = "Delete from Books where books_sku=?";
        //----------------sku parameter---------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Book->GetSku());
        $params[] = $param;
        //----------------pass to execute--------------------
        self::Executesql($SqlStatement,$params);
        if(self::$affected_rows>0){
            self::DeleteProduct($Book);
            if(self::$affected_rows>0)
                return true; 
        }
        return false; 
    }
    //Get count of all products or check existance of specific Book
    public static function BooksCount($Book=null)
    {
        $SqlStatement = "select count(books_sku) as c from books ";
        if ($Book!=null ) {
            $SqlStatement .= "where books_sku=?";
            //----------------sku parameter---------------------
            $param = new SQLParameter();
            $param->SetName("sku");
            $param->SetType("s");
            $param->SetValue($Book->GetSku());
            $params[] = $param;
        }
        //-----------------pass to execute------------------
        return self::Executesql($SqlStatement,$params);
    }
    //****************************DVDS*************************************** */
    //here we will return all dvds
    public static function GetAllDVDs()
    {
        $SqlStatement = " SELECT product_sku,product_name,price,type, size
                        FROM dvds,products
                        where dvds.DVD_sku=products.product_sku;";
        //--------------pass to execute--------------------
        $result = self::ExecuteSql($SqlStatement);
        while ($row=$result->fetch_assoc()) {
          $DVD = new DVD($row['product_sku'], $row['product_name'], $row['price'], $row['size']);
          $DVDs[] = $DVD;
        }
        return $DVDs;
    } 
    //here we will return specific DVD by its Sku
    public static function GetDVDBySku($DVD)
    {
        $SqlStatement = "SELECT product_sku,product_name,price,type, size
                         FROM dvds,products
                         where dvds.DVD_sku=products.product_sku
                         And   dvds.DVD_sku= ?";
        //--------------sku parameter--------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($DVD->GetSku());
        $params[] = $param;
        //-----------------pass to execute-----------------
        $result = self::ExecuteSql($SqlStatement, $params);
        $row = $result->fetch_assoc();
        if ($row) {
            //----------fill DVD with data -----------
            $DVD->SetName($row['product_name']) ;
            $DVD->SetPrice($row['price']) ;
            $DVD->setSize($row['size']) ;
            return true;
        }
        return false;
    }
    //insert a DVD in the database
    public static function InsertDVD($DVD)
    {
        //first insert this DVD in product table 
        if (self::InsertProduct($DVD))
        {
            //if it was inserted successfully insert it
            //in DVDs table
            $SqlStatement = "insert into DVDs(DVD_sku,size)
                             Values (?,?)";
            //----------------sku parameter-----------------------------------
            $param = new SQLParameter();
            $param->SetName("sku");
            $param->SetType("s");
            $param->SetValue($DVD->GetSku());
            $params[] = $param;
            //---------------size parameter------------------------------------
            $param1 = new SQLParameter();
            $param1->SetName("size");
            $param1->SetType("d");
            $param1->SetValue($DVD->GetSize());
            $params[] = $param1;
            //----------------pass to execute----------------------------------
            self::Executesql($SqlStatement, $params);
            if(self::$affected_rows>0)
                return true;
        }
        return false;
   }
    //insert a new DVD in the database by using Stored procedure
    public static function InsertDVDBySP($DVD)
    {
        $SqlStatement = "CALL InsertDVD (?,?,?,?,?)";
        //----------------sku parameter-----------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($DVD->GetSku());
        $params[] = $param;
        //----------------product_name parameter-----------------------------------
        $param1 = new SQLParameter();
        $param1->SetName("product_name");
        $param1->SetType("s");
        $param1->SetValue($DVD->GetName());
        $params[] = $param1;
        //----------------price Parameter------------------------------------
        $param2 = new SQLParameter();
        $param2->SetName("price");
        $param2->SetType("d");
        $param2->SetValue($DVD->GetPrice());
        $params[] = $param2;
        //----------------Type Parameter------------------------------------
        $param3 = new SQLParameter();
        $param3->SetName("Type");
        $param3->SetType("s");
        $param3->SetValue($DVD->GetType());
        $params[] = $param3;
        //-----------------Size parameter----------------------------------
        $param4 = new SQLParameter();
        $param4->SetName("Size");
        $param4->SetType("d");
        $param4->SetValue($DVD->GetSize());
        $params[] = $param4;
        //------------------pass to execute----------------------------------  
        self::Executesql($SqlStatement, $params);
        if(self::$affected_rows>0)
            return true;
        return false;
    }
    //delete DVD from database
    public static function DeleteDVD($DVD)
    {
        $SqlStatement = "Delete from DVDs where DVD_sku=?";
        //------------------sku parameter-------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($DVD->GetSku());
        $params[] = $param;
        //----------------pass to execute--------------------------------
        self::Executesql($SqlStatement, $params);
        if(self::$affected_rows>0)
        {
            //delete DVD from product table
            self::DeleteProduct($DVD);
            if(self::$affected_rows>0)
                return true;
        }
        return false;              
    }
    //Get count of all DVDs or check existance of specific DVD
    public static function DVDsCount($DVD = null)
    {
       $SqlStatement = "select count(DVD_sku) as c from dvds ";
       if ($DVD!=null ) {
           $SqlStatement .= "where DVD_sku=?";
           //----------sku parameter -----------
           $param = new SQLParameter();
           $param->SetName("sku");
           $param->SetType("s");
           $param->SetValue($DVD->GetSku());
           $params[] = $param;
        }
        //------------------pass to execute-------------------
        return  self::Executesql($SqlStatement, $params);
    }
    /************************Furnitures***************************************/ 
    //return All furniture in the Database
    public static function GetAllFurnitures()
    {
        $SqlStatement = "SELECT product_sku,product_name,price,type, height,width,length
                         FROM furnitures,products
                         where furnitures.furniture_sku=products.product_sku;";
        //------------------pass to execute-------------------
        $result = self::ExecuteSql($SqlStatement);
        while ($row = $result->fetch_assoc()) {
            $Furniture = new Furniture($row['product_sku'], $row['product_name'], $row['price'], $row['height'], $row['width'], $row['length']);
            $Furnitures[] = $Furniture;
        }
        return $Furnitures;
    } 
    //here we will return specific Furniture by its Sku
    public static function GetFurnitureBySku($Furniture)
    { 
        $SqlStatement = "SELECT product_sku,product_name,price,type, height,width,length
                        FROM   furnitures,products
                        where  furnitures.furniture_sku=products.product_sku
                        And    furnitures.furniture_sku=?";
        //----------------sku parameter-----------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Furniture->GetSku());
        $params[] = $param;
        //------------------pass to execute-------------------
        $result=self::ExecuteSql($SqlStatement,$params);
        $row=$result->fetch_assoc() ;
        if ($row) {
            //----------fill Furniture with other data -----------
            $Furniture->SetName($row['product_name']) ;
            $Furniture->SetPrice($row['price']) ;
            $Furniture->setHeight($row['height']) ;
            $Furniture->setwidth($row['width']) ;
            $Furniture->setlength($row['length']) ;
            return true;
        }
        return false;
    } 
    //insert Furniture in the database
    public static function InsertFurniture($Furniture)
    {
        //first insert this Furniture in product table 
        if (self::InsertProduct($Furniture)) {
            //if it was inserted successfully insert it
            //in Furnitures table
            $SqlStatement = "insert into furnitures(furniture_sku,height,width,length)
                             Values (?,?,?,?)";
            //----------------sku parameter--------------------------------------
            $param = new SQLParameter();
            $param->SetName("sku");
            $param->SetType("s");
            $param->SetValue($Furniture->GetSku());
            $params[] = $param;
            //----------------height parameter-----------------------------------
            $param1 = new SQLParameter();
            $param1->SetName("height");
            $param1->SetType("i");
            $param1->SetValue($Furniture->GetHeight());
            $params[] = $param1;
            //-----------------width parameter-----------------------------------
            $param2 = new SQLParameter();
            $param2->SetName("width");
            $param2->SetType("i");
            $param2->SetValue($Furniture->GetWidth());
            $params[] = $param2;
            //-----------------length parameter-----------------------------------
            $param3 = new SQLParameter();
            $param3->SetName("length");
            $param3->SetType("i");
            $param3->SetValue($Furniture->GetLength());
            $params[] = $param3;
            //-----------------pass to execute------------------------------------ 
            self::Executesql($SqlStatement, $params);
            if(self::$affected_rows>0)
                return true;           
        }
        return false;
    }
    //insert a new Furniture in the database by using Stored procedure
    public static function InsertFurnitureBySP($Furniture)
    {
        $SqlStatement="CALL Insertfurniture (?,?,?,?,?,?,?)";
        //----------------sku parameter-----------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Furniture->GetSku());
        $params[] = $param;
        //----------------product_name parameter-----------------------------------
        $param1 = new SQLParameter();
        $param1->SetName("product_name");
        $param1->SetType("s");
        $param1->SetValue($Furniture->GetName());
        $params[] = $param1;
        //----------------price Parameter------------------------------------
        $param2 = new SQLParameter();
        $param2->SetName("price");
        $param2->SetType("d");
        $param2->SetValue($Furniture->GetPrice());
        $params[] = $param2;
        //----------------Type Parameter------------------------------------
        $param3 = new SQLParameter();
        $param3->SetName("Type");
        $param3->SetType("s");
        $param3->SetValue($Furniture->GetType());
        $params[] = $param3;
        //----------------height parameter-----------------------------------
        $param4 = new SQLParameter();
        $param4->SetName("height");
        $param4->SetType("i");
        $param4->SetValue($Furniture->GetHeight());
        $params[] = $param4;
        //-----------------width parameter-----------------------------------
        $param5 = new SQLParameter();
        $param5->SetName("width");
        $param5->SetType("i");
        $param5->SetValue($Furniture->GetWidth());
        $params[] = $param5;
        //-----------------length parameter-----------------------------------
        $param6 = new SQLParameter();
        $param6->SetName("length");
        $param6->SetType("i");
        $param6->SetValue($Furniture->GetLength());
        $params[] = $param6;
        //------------------pass to execute----------------------------------  
        self::Executesql($SqlStatement,$params);
        if(self::$affected_rows>0)
            return true;
        return false;
    }
    //delete Furniture from database
    public static function DeleteFurniture($Furniture)
    {
        $SqlStatement = "Delete from furnitures where furniture_sku=?";
        //-----------------sku parameter----------------------------------
        $param = new SQLParameter();
        $param->SetName("sku");
        $param->SetType("s");
        $param->SetValue($Furniture->GetSku());
        $params[] = $param;
        //---------------pass to execute------------------------------------
        self::Executesql($SqlStatement,$params);
        if(self::$affected_rows>0){
            //delete furniture from product table
            self::DeleteProduct($Furniture);
            if(self::$affected_rows>0)
            return true;
        }
        return false;
    }
    //Get count of all furnitures or check existance of specific furniture
    public static function FurnituresCount($Furniture=null)
    {
         $SqlStatement = "select count(furniture_sku) as c from furnitures ";
         if($Furniture != null ){
              $SqlStatement .= "where furniture_sku=?";
              $param = new SQLParameter();
              $param->SetName("sku");
              $param->SetType("s");
              $param->SetValue($Furniture->GetSku());
              $params[] = $param;
          }
          return  self::Executesql($SqlStatement,$params);
    }
}

class SQLParameter
{
    private $Name;
    private $Type;
    private $Value;
    
    public function GetName()
    {
        return "$".$this->Name;
    }
    public function SetName($_Name)
    {
        $this->Name=$_Name;
    }
    public function GetType()
    {
        return $this->Type;
    }
    public function SetType($_Type)
    {
        $this->Type=$_Type;
    }
    public function GetValue()
    {
        return $this->Value;
    }
    public function SetValue($_Value)
    {
        $this->Value=$_Value;
    }
}
    
class MySqlErrors
{
    private static $Errors=Array (
    1062=>'Please insert another Sku, this poduct already existed ',
    0=>'');
    public static function Get_error()
    {
        $ErrorNo= DAL::Get_error();
        return  self::$Errors[$ErrorNo];
    }
}
