import { createRouter, createWebHistory } from 'vue-router'

import AddProduct from '../views/AddProduct.vue'
import AllProducts from '../views/AllProducts.vue'

const routes = [
  {
    path: '/addproduct',
    name: 'AddProduct',
    component:  AddProduct
  },
  {
    path: '/',
    name: 'AllProducts',
    component:AllProducts
  }
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
export default router
